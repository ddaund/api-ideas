package dd.boot2.cars.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import dd.boot2.cars.entity.VehicleEO;

public interface VehicleRepository extends CrudRepository<VehicleEO, UUID> {

    List<VehicleEO> findByMake(@Param("make") String make);
}