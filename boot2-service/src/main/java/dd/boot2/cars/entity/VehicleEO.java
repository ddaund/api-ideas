package dd.boot2.cars.entity;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.envers.Audited;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Audited
public class VehicleEO {

    @Id
    private UUID id;
    private String vehicleIdentityNumber;
    private String make;
    private String model;
    private String status;

}
