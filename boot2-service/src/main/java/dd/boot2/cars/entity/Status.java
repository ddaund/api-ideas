package dd.boot2.cars.entity;

public enum Status {
    FOR_SALE, SOLD
}