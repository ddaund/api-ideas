package dd.boot2.commons.entity;

public interface CodeEnum {
    String code();
    String display();
}
