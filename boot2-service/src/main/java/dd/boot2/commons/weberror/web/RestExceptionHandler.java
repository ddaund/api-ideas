package dd.boot2.commons.weberror.web;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import javax.validation.ConstraintViolationException;

import org.hibernate.PropertyValueException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import dd.boot2.commons.exception.EntityNotFoundException;
import dd.boot2.commons.weberror.model.ApiError;
import lombok.extern.slf4j.Slf4j;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@Slf4j
//https://www.toptal.com/java/spring-boot-rest-api-error-handling
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected String handleCustomException(Exception ce) {
        return ce.getClass().getSimpleName() + " : " + ce.getMessage();
    }

// custom defined exceptions handler

    /**
     * Handles EntityNotFoundException. Created to encapsulate errors with more detail than javax.persistence.EntityNotFoundException.
     */
    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(EntityNotFoundException ex) {
        return buildResponseEntity(ApiError.builder().status(NOT_FOUND).message(ex.getMessage()).build());
    }

// springframework defined exceptions

    /**
     * Handle javax.persistence.EntityNotFoundException
     */
    @ExceptionHandler(javax.persistence.EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(javax.persistence.EntityNotFoundException ex) {
        return buildResponseEntity(ApiError.builder().status(HttpStatus.NOT_FOUND).debugMessage(ex.getLocalizedMessage()).build());
    }

    /**
     * Handles Exception. Thrown when or.springframework.util.Assert or Objects.requireNonNull()
     */
    @ExceptionHandler({IllegalStateException.class, IllegalArgumentException.class, NullPointerException.class})
    protected ResponseEntity<Object> handleIllegalStateException(Exception ce) {

        return buildResponseEntity(ApiError.builder().status(BAD_REQUEST).message(ce.getMessage()).build());
    }

    /**
     * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
     */
    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> onConstraintValidationException(ConstraintViolationException ex) {

        ApiError apiError = ApiError.builder().status(BAD_REQUEST).message("Validation Error").build();
        apiError.addValidationErrors(ex.getConstraintViolations());
        return buildResponseEntity(apiError);
    }

    /**
     * Handle DataIntegrityViolationException, inspects the cause for different DB causes.
     */
    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolation(DataIntegrityViolationException ex,
                                                                  WebRequest request) {
        if (ex.getCause() instanceof ConstraintViolationException) {
            return buildResponseEntity(ApiError.builder().status(HttpStatus.CONFLICT).message("Database error caused by ConstraintViolationException.class").debugMessage(ex.getCause().getMessage()).build());
        } else if(ex.getCause() instanceof PropertyValueException){
            return buildResponseEntity(ApiError.builder().status(HttpStatus.CONFLICT).message("Database error caused by PropertyValueException.class").debugMessage(ex.getCause().getMessage()).build());
        }
        return buildResponseEntity(ApiError.builder().status(HttpStatus.INTERNAL_SERVER_ERROR).debugMessage(ex.getLocalizedMessage()).build());
    }

    /**
     * handle generic Exception.class
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      WebRequest request) {
        ApiError apiError = ApiError.builder().status(BAD_REQUEST).debugMessage(ex.getMessage()).message(String.format("The parameter '%s' of value '%s' could not be converted to type '%s'", ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName())).build();

        return buildResponseEntity(apiError);
    }

    /**
     * private method to build ResponseEntity with body (apiError) and status (apiError.status)
     */
    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

// Overridden methods below for more information than the standard handling in base class ResponseEntityExceptionHandler.class

    /**
     * Triggered when a 'required' request parameter is missing.
     */
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " parameter is missing";
        return buildResponseEntity(ApiError.builder().status(BAD_REQUEST).debugMessage(ex.getLocalizedMessage()).message(error).build());
    }

    /**
     * This one triggers when JSON is invalid as well.
     */
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));

        ApiError apiError = ApiError.builder()
                .status(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
                .message(builder.substring(0, builder.length() - 2))
                .debugMessage(ex.getLocalizedMessage()).build();

        return buildResponseEntity(apiError);
    }

    /**
     * Triggered when an object fails @Valid validation.
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        ApiError apiError = ApiError.builder().status(BAD_REQUEST).message("Validation error").build();

        apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
        apiError.addValidationError(ex.getBindingResult().getGlobalErrors());
        return buildResponseEntity(apiError);
    }

    /**
     * Happens when request JSON is malformed.
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ServletWebRequest servletWebRequest = (ServletWebRequest) request;
        log.info("{} to {}", servletWebRequest.getHttpMethod(), servletWebRequest.getRequest().getServletPath());
        String error = "Malformed JSON request";
        return buildResponseEntity(ApiError.builder().status(HttpStatus.BAD_REQUEST).message(error).debugMessage(ex.getLocalizedMessage()).build());
    }

    /**
     * Handle HttpMessageNotWritableException.
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        String error = "Error writing JSON output";
        return buildResponseEntity(ApiError.builder().status(HttpStatus.INTERNAL_SERVER_ERROR).message(error).debugMessage(ex.getLocalizedMessage()).build());
    }

    /**
     * Handle NoHandlerFoundException.
     */
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApiError apiError = ApiError.builder().status(BAD_REQUEST).message(String.format("Could not find the %s method for URL %s", ex.getHttpMethod(), ex.getRequestURL())).debugMessage(ex.getMessage()).build();
        return buildResponseEntity(apiError);
    }
}