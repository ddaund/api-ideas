package dd.boot2.commons.aspectj;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import lombok.extern.slf4j.Slf4j;

@Component
@Aspect
@Slf4j
public class MethodPerformance {

    @Around("@annotation(MethodTiming)")
    public Object calculateMethodMetric(ProceedingJoinPoint joinPoint) throws Throwable{

        Object obj = null;
        long startTime = System.currentTimeMillis();

        StopWatch stopWatch = new StopWatch();
        final String methodName = joinPoint.getSignature().getName();

        log.info("methodName: " + methodName + " startTime: " + startTime);

        try{
            stopWatch.start();
            obj = joinPoint.proceed(joinPoint.getArgs());
        } finally {
            stopWatch.stop();
            long endTime = System.currentTimeMillis();
            long duration = endTime - startTime;

            log.info("methodName: " + methodName + " endTime: " + endTime);
            log.info("methodName: " + methodName + " duration: " + duration);
        }
        return obj;
    }
}
