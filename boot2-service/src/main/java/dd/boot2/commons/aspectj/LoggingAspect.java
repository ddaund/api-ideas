package dd.boot2.commons.aspectj;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
public class LoggingAspect {

    @Around("@annotation(LogTransactionSLA)")
    public Object logSLA(   ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        Object object = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Signature signature = proceedingJoinPoint.getSignature();
        try {
            object = proceedingJoinPoint.proceed();
        } catch (Throwable ex) {
            log.error("Error processing request ", ex);
            throw ex;
        } finally {
            stopWatch.stop();
            log.info("Total time to run - " + signature.getDeclaringTypeName() + "." + signature.getName()
                    + " method = " + stopWatch.getTotalTimeMillis() + " seconds.");
        }
        return object;
    }
}
