package dd.boot.employee.domain.entity;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class EmployeeEORepositoryTest {

	@Autowired
	private EmployeeEORepository repo;

	@Test
	public void test() {

		assertThat(repo).isNotNull();

		assertThat(repo.findByFirstNameNot("Digambar")).size().isEqualTo(1);
		assertThat(repo.findByFirstNameIsNot("Digambar")).size().isEqualTo(1);
		assertThat(repo.findAll()).size().isEqualTo(2);
		assertThat(repo.findByFirstNameStartingWith("Pras")).size().isEqualTo(1); // we don't need to add % operator
																					// inside the argument
	}
}
