--Spring Boot runs schema-@@platform@@.sql automatically during startup. -all is the default for all platforms.

INSERT INTO address(street1, state, city, postal_code, country) VALUES
('Standford Ave', 'CA', 'Los Angeles',  '90001', 'USA'),
('Elite Empire', 'MH', 'Pune',  '411045', 'IND');

INSERT INTO employee (first_name, last_name, mobile, email) VALUES
('Digambar', 'Daund', '9890319921', 'digambar.daund@infosys.com'),
('Prashant', 'Swamy', '8459595762', 'prashant.swamy@infosys.com');

INSERT INTO employee_address(employee_id, address_id, type, use) VALUES
(1, 2 , 'both', 'home'),
(2, 1 , 'both', 'home');

INSERT INTO book (isbn13, language, num_of_pages, price, publish_date, publisher, title, type) VALUES
('isbn', 'marathi', 10, 100, '2020-06-02', 'infosys', 'how to get full variable pay', 'blog');


