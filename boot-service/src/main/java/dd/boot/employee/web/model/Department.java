package dd.boot.employee.web.model;

import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Department {

    private Long id;
    @NotBlank
    private String name;
    private Employee manager;
    private List<Employee> employees;
}
