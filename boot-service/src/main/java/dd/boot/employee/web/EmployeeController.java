package dd.boot.employee.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dd.boot.employee.domain.entity.EmployeeEO;
import dd.boot.employee.web.model.Employee;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/employee")
@Validated
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value = "/{id}")
    public Employee findById(@PathVariable Long id) {
        return employeeService.findById(id);
    }

    @GetMapping(params = "firstName")
    public List<EmployeeEO> findByFirstName(@RequestParam String firstName) {
        return employeeService.findByFirstName(firstName);
    }

    @GetMapping(params = "lastName")
    public List<Employee> findByLastName(@RequestParam String lastName) {
        System.out.println(employeeService.findByLastName(lastName));
        return employeeService.findByLastName(lastName);
    }

    @PostMapping
    public EmployeeEO createEmployee(@RequestBody EmployeeEO employee) {
        log.debug("EmployeeController.create() thread name - " + Thread.currentThread().getName());
        return employeeService.create(employee);
    }

    @DeleteMapping
    public void deleteAllEmployeesInBatch() {
        log.debug("EmployeeController.deleteAllUsersInBatch() thread name - " + Thread.currentThread().getName());
        employeeService.deleteAllInBatch();
    }
}
