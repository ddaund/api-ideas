package dd.boot.employee.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dd.boot.employee.domain.entity.EmployeeEO;
import dd.boot.employee.domain.entity.EmployeeEORepository;
import dd.boot.employee.web.model.Employee;
import dd.boot.library.web.error.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmployeeService {

    @Autowired
    private EmployeeEORepository employeeEORepository;

    public Employee findById(Long id) {

        Optional<EmployeeEO> employee = employeeEORepository.findById(id);
        if(employee.isPresent()){
            Employee response=null;
            System.out.println(employee.get());
            return response;
        } else{
            throw new EntityNotFoundException(EmployeeEO.class, String.valueOf(id));
        }
    }

    public List<EmployeeEO> findByFirstName(String firstName) {

        return employeeEORepository.findByFirstName(firstName);
    }

    public List<Employee> findByLastName(String lastName) {

        List<Employee> response = new ArrayList<>();
        List<EmployeeEO> employees = employeeEORepository.findByLastName(lastName);
        System.out.println(employees);
        return response;
    }

    @Transactional
    public EmployeeEO create(EmployeeEO employee) {

        log.debug("EmployeeService.create() thread name - " + Thread.currentThread().getName());
        return employeeEORepository.save(employee);
    }

    @Transactional
    @Async
    public void deleteAllInBatch() {

        log.debug("EmployeeService.deleteAllInBatch() thread name -  " + Thread.currentThread().getName());
        employeeEORepository.deleteAllInBatch();
    }



    /**
     * deleting single entity this way make sure all related owned entities are also deleted
     * Ex. OneToOne(cascade = CascadeType.ALL) on EmployeeEO will make sure enclosing entity record is also removed.
     */
    public void deleteById(Long personId) {
        employeeEORepository.deleteById(personId);
    }
}
