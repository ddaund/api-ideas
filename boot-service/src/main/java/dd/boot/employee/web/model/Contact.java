package dd.boot.employee.web.model;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Contact {

    private Long id;
    @NotBlank
    private String name;
    @Valid
    private Address address;
    @NotBlank @Digits(integer = 11, fraction = 0)
    private String phoneNumber;
    @Email
    private List<String> emails;

    private String type;
    private String status;

    @AssertTrue (message = "PhoneNumber Or Email must not be Blank.")
    private boolean isNotBlankPhoneNumberOrEmail(){
        return StringUtils.isNotBlank(phoneNumber) || CollectionUtils.isNotEmpty(emails);
    }

}
