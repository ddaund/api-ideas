package dd.boot.employee.web.model;

import java.time.LocalDate;
import java.util.List;

import dd.boot.employee.domain.entity.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Employee {

    private Long id;
    private LocalDate birthDate;
    private String firstName;
    private String lastName;
    private Gender gender;
    private LocalDate hireDate;

    private List<Address> addresses;
    private List<Contact> contacts;
    private Department department;

}