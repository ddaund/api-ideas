package dd.boot.employee.web.model;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Address {

	private Long id;
	@NotEmpty
	private String street1;
	private String street2;
	@NotEmpty
	private String city;
	@NotEmpty
	private String state;
	@NotEmpty
	private String postalCode;
	private String country;

	private String type;
	private String status;
}
