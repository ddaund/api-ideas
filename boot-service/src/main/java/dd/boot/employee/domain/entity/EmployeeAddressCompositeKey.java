package dd.boot.employee.domain.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
public class EmployeeAddressCompositeKey implements Serializable {

	private static final long serialVersionUID = 8485604435132184782L;
	@ToString.Exclude
    private EmployeeEO employeeEO;
    private AddressEO addressEO;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof EmployeeAddressEO)) {
            return false;
        }
        return (employeeEO.getId() != null && employeeEO.getId().equals(((EmployeeAddressEO) o).getEmployeeEO().getId()))
                &&(addressEO.getId() != null && addressEO.getId().equals(((EmployeeAddressEO) o).getAddressEO().getId()));
    }
}
