package dd.boot.employee.domain.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum AddressUse implements DbCodeEnum<String> {
    HOME("home", "Home"),
    WORK("work", "Work"),
    TEMP("temp", "Temporary"),
    OLD("old", "Old/Incorrect"),
    BILLING("billing", "Billing");

    private static final Map<String, AddressUse> BY_CODE = new HashMap<>();
    private static final Map<String, AddressUse> BY_LABEL = new HashMap<>();

    static {
        for (AddressUse addressUse : values()) {
            BY_CODE.put(addressUse.dbCode, addressUse);
            BY_LABEL.put(addressUse.label, addressUse);
        }
    }

    private final String dbCode;
    private final String label;

    AddressUse(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static AddressUse valueOfCode(String dbCode) {
        return BY_CODE.get(dbCode);
    }

    public static AddressUse valueOfDisplay(String label) {
        return BY_LABEL.get(label);
    }

    @Override
    public String dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
