package dd.boot.employee.domain.entity;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeEORepository extends JpaRepository<EmployeeEO, Long> {

//    /* Number Keywords: And, Or, LessThan, LessThanEqual, GRaterThan, GraterThanEqual */
//    List<EmployeeEO> findByDepartment(String department);
//    List<EmployeeEO> findBySalaryLessThan(Long salary);
//    List<EmployeeEO> findByDepartmentAndSalaryLessThan(String dept, Long salary);
    List<EmployeeEO> findByLastNameAndFirstName(String firstName, String lastName);
    List<EmployeeEO> findByLastNameOrFirstName(String firstName, String lastName);
//    List<EmployeeEO> findByAgeLessThan(Integer page);
//    List<EmployeeEO> findByAgeLessThanEqual(Integer page);
//    List<EmployeeEO> findByAgeGreaterThan(Integer page);
//    List<EmployeeEO> findByAgeGreaterThanEqual(Integer page);

    /* Date Keywords: Between, After, Before*/
    List<EmployeeEO> findByHireDateBetween(LocalDate from, LocalDate pto);
    List<EmployeeEO> findByHireDateAfter(LocalDate from);
    List<EmployeeEO> findByHireDateBefore(LocalDate from);

//    /* Null Keywords: IsNull, IsNotNull, NotNull*/
//    List<EmployeeEO> findByAgeIsNull();
//    List<EmployeeEO> findByAgeNotNull();
//    List<EmployeeEO> findByAgeIsNotNull();
//
    /*String Keywords: Is, Equals, Not, IsNot, Like, NotLike */
    List<EmployeeEO> findByFirstNameIgnoreCase(String firstName);
    List<EmployeeEO> findByFirstName(String firstName);
    List<EmployeeEO> findByLastName(String lastName);
    List<EmployeeEO> findByFirstNameIs(String firstName);
    List<EmployeeEO> findByFirstNameEquals(String firstName);
    List<EmployeeEO> findByFirstNameNot(String firstName);
    List<EmployeeEO> findByFirstNameIsNot(String firstName);
    List<EmployeeEO> findByFirstNameLike(String firstName);
    List<EmployeeEO> findByFirstNameNotLike(String firstName);

    /*String LIKE Keywords: EndingWith, Containing - StartingWith we don't need to add % operator inside the argument*/
    List<EmployeeEO> findByFirstNameStartingWith(String pname);
    List<EmployeeEO> findByFirstNameEndingWith(String pname);
    List<EmployeeEO> findByFirstNameContaining(String pname);

//    /* OrderBy */
//    List<EmployeeEO> findByAgeOrderByLastNameDesc(String pname);
//
//    /* Number List In, NotIn, */
//    List<EmployeeEO> findByAgeIn(Collection<Integer> ages);
//    List<EmployeeEO> findByAgeNotIn(Collection<Integer> ages);
//
//    /* boolean True, False */
//    List<EmployeeEO> findByActiveTrue();
//    List<EmployeeEO> findByActiveFalse();

}