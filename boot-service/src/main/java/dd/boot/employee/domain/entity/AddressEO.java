package dd.boot.employee.domain.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "Address")
public class AddressEO implements Serializable {

	private static final long serialVersionUID = 5409908953921923897L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String street1;
    private String street2;
    private String city;
    private String state;
    private String postalCode;
    private String country;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AddressEO)) {
            return false;
        }
        return id != null && id.equals(((AddressEO) o).getId());
    }

    public int hashCode() {
        return id.hashCode();
    }
}