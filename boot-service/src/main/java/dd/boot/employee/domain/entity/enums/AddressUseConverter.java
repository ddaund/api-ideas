package dd.boot.employee.domain.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class AddressUseConverter implements AttributeConverter<AddressUse, String> {

    @Override
    public String convertToDatabaseColumn(AddressUse addressUse) {

        if (addressUse == null) return null;
        return addressUse.dbCode();
    }

    @Override
    public AddressUse convertToEntityAttribute(String code) {
        return AddressUse.valueOfCode(code);
    }
}