package dd.boot.employee.domain.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import dd.boot.employee.domain.entity.enums.AddressType;
import dd.boot.employee.domain.entity.enums.AddressUse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@IdClass(EmployeeAddressCompositeKey.class)
@Table(name = "EmployeeAddress")
@Entity /*One to Many Unidirectional with relationship property type and use */
public class EmployeeAddressEO implements Serializable {

	private static final long serialVersionUID = -2574923552149299981L;

	@ToString.Exclude
    @Id
    @ManyToOne
    @JoinColumn(name="employee_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_employee_address_employee_id"))
    private EmployeeEO employeeEO;

    @Id
    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_employee_address_address_id"))
    private AddressEO addressEO;

    /* enums */
    private AddressType type;
    private AddressUse use;
}
