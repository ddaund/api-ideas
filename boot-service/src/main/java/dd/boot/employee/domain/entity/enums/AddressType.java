package dd.boot.employee.domain.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum AddressType implements DbCodeEnum<String> {
    POSTAL("postal", "Postal"),
    PHYSICAL("physical", "Physical"),
    BOTH("both", "Postal & Physical");

    private static final Map<String, AddressType> BY_CODE = new HashMap<>();
    private static final Map<String, AddressType> BY_DISPLAY = new HashMap<>();

    static {
        for (AddressType value : values()) {
            BY_CODE.put(value.code, value);
            BY_DISPLAY.put(value.display, value);
        }
    }

    private final String code;
    private final String display;

    AddressType(String code, String display) {
        this.code = code;
        this.display = display;
    }

    public static AddressType valueOfCode(String code) {
        return BY_CODE.get(code);
    }

    public static AddressType valueOfDisplay(String display) {
        return BY_DISPLAY.get(display);
    }

    @Override
    public String dbCode() {
        return code;
    }

    @Override
    public String label() {
        return display;
    }
}
