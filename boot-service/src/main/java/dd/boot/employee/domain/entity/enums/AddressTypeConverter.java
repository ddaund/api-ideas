package dd.boot.employee.domain.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class AddressTypeConverter implements AttributeConverter<AddressType, String> {

    @Override
    public String convertToDatabaseColumn(AddressType addressType) {

        if (addressType == null) return null;
        return addressType.dbCode();
    }

    @Override
    public AddressType convertToEntityAttribute(String code) {
        return AddressType.valueOfCode(code);
    }
}