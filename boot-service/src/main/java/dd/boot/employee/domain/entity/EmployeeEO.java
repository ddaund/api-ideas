package dd.boot.employee.domain.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import dd.boot.employee.domain.entity.enums.Gender;
import dd.boot.library.entity.envers.Auditable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "Employee")
public class EmployeeEO extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 2077360931628670105L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //mysql
    private LocalDate birthDate;
    private String firstName;
    private String lastName;
    private Gender gender;
    private LocalDate hireDate;

    @OneToMany(mappedBy = "employeeEO", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<EmployeeAddressEO> employeeAddressEOList = new ArrayList<>();


//    @OneToMany
//    private List<TitleEO> titleEOs;

//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
//    @JoinColumn(name = "departmentId", updatable = false, insertable = false)
//    private DepartmentEO departmentEO;
//
//    @ManyToOne(fetch = FetchType.EAGER, optional = false)
//    @JoinColumn(name = "managerId", updatable = false, insertable = false)
//    private EmployeeEO managerEO;

    private Date endDate;
    private String email;
    private String mobile;
    private String workstation;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof EmployeeEO)) {
            return false;
        }
        return id != null && id.equals(((EmployeeEO) o).getId());
    }

    public int hashCode() {
        return id.hashCode();
    }
}
