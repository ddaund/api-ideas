package dd.boot.employee.domain.entity.enums;


import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum Gender implements DbCodeEnum<String> {
    M("male", "Male"),
    F("female", "Female"),
    O("other", "Other"),
    U("unknown", "Unknown");

    private static final Map<String, Gender> BY_CODE = new HashMap<>();
    private static final Map<String, Gender> BY_DISPLAY = new HashMap<>();

    static {
        for (Gender value : values()) {
            BY_CODE.put(value.dbCode, value);
            BY_DISPLAY.put(value.label, value);
        }
    }

    private final String dbCode;
    private final String label;

    Gender(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static Gender valueOfCode(String dbCode) {
        return BY_CODE.get(dbCode);
    }

    public static Gender valueOfDisplay(String label) {
        return BY_DISPLAY.get(label);
    }

    @Override
    public String dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
