package dd.boot.book.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;


@Data
public class BookDto {
	
    private Long id;
    @NotEmpty
    private String title;
    @PastOrPresent
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    // custom pattern @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate publishDate;
    private String author;
    private String type;
    private BookType booktype;
    private double price;
    private int numOfPages;
    private String publisher;
    private String language;
    private String isbn13;
}
