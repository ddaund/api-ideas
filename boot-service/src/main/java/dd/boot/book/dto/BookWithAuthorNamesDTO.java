package dd.boot.book.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  you can use it as a DTO model with JPQL, criteria and native queries
 */
//https://thorben-janssen.com/dto-projections/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookWithAuthorNamesDTO {

    private Long bookId;
    private String bookTitle;
    private Double bookPrice;
    private String authorNames;

}
