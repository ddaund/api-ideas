package dd.boot.book.dto;

public enum BookType {
    EBOOK("eBook"), HARDCOPY("Hard copy");

    public final String label;

    BookType(String label){
        this.label = label;
    }
}
