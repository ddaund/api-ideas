package dd.boot.book.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookTupleProjectionJpqlQueryRepository extends CrudRepository<BookEO, Long> {

    
}
