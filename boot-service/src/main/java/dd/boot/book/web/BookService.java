package dd.boot.book.web;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dd.boot.book.entity.BookEO;
import dd.boot.book.entity.BookRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public List<BookEO> getAllBooks() {
        
    	log.info("getAllBooks method called");
        return bookRepository.findAll();
    }

    public BookEO getBook(Long id) {
        
    	return bookRepository.findById(id).orElseGet(BookEO::new);
    }

    public void addBook(BookEO whiskey) {
        
    	bookRepository.save(whiskey);
    }

    public void updateBook(Long id, BookEO whiskey) {
        
    	bookRepository.save(whiskey);
    }

    public void deleteBook(Long id) {
        Optional<BookEO> book = bookRepository.findById(id);
        if(book.isPresent()) {
        	bookRepository.deleteById(id);
        } else {
        	throw new EntityNotFoundException("No book found with id = " +id);
        }
    }
}
