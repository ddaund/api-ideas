package dd.boot.book.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dd.boot.book.dto.BookDto;
import dd.boot.book.entity.BookEO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/book")
@Validated
public class BookController {

    @Autowired
    private BookService bookservice;

    private ModelMapper modelMapper = new ModelMapper();

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<BookDto>> getAllBooks() {

        log.info("inside getAllBooks");
        List<BookEO> entities = bookservice.getAllBooks();
        List<BookDto> bookDtos = entities.parallelStream().map(this::convert).collect(Collectors.toList());
        return new ResponseEntity<>(bookDtos, HttpStatus.OK);//200 OK
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<?> getBook(@PathVariable("id") Long id) {

        BookEO entity = bookservice.getBook(id);
        return new ResponseEntity<>(convert(entity), HttpStatus.OK); //200 OK
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED) //201 Created
    public void addBook(@RequestBody @Valid BookDto bookDto) {

        bookservice.addBook(convert(bookDto));
    }

    @PutMapping("/{bookId}")
    @ResponseStatus(HttpStatus.NO_CONTENT) //204 No Content - updates that dont return data to client else use 200 OK
    public void updateBook(@PathVariable Long id, @RequestBody BookDto dto) {

        bookservice.updateBook(id, convert(dto));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) //204 No Content - deletes that dont return data to client else use 200 OK
    public void deleteBook(@PathVariable("id") Long id) {

        bookservice.deleteBook(id);
    }

    @GetMapping(value = "/exception")
    public ResponseEntity<?> exception() {

        throw new NullPointerException();
    }

    private BookDto convert(BookEO entity) {

        return modelMapper.map(entity, BookDto.class);
    }

    private BookEO convert(BookDto dto) {

        return modelMapper.map(dto, BookEO.class);
    }
    
    /*
    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED) //202 Accepted - Async call denoting request accepted for processing but not yet completed.
    public void processAsync(){

    }*/
}
