package dd.boot.ecart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Order_Detail")
public class OrderDetailEO implements Serializable {

    private static final long serialVersionUID = 7550745928843183535L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "order_id", nullable = false, foreignKey = @ForeignKey(name = "FK_orderdetail_order"))
//    private OrderEO orderEO;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id", nullable = false, foreignKey = @ForeignKey(name = "FK_orderdetail_product"))
    private ProductEO productEO;

    @Column(nullable = false)
    private int quanity;

    @Column(nullable = false)
    private double price;

    @Column(nullable = false)
    private double amount;

}