package dd.boot.ecart.entity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetailCmdRepository extends JpaRepository<OrderDetailEO, Long> {
}
