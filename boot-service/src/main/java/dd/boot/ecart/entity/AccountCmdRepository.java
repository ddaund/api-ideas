package dd.boot.ecart.entity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountCmdRepository extends JpaRepository<AccountEO, Long> {

}
