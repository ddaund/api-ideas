//package dd.boot.ecart.entity;
//
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import lombok.ToString;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.sql.Date;
//
//@Getter
//@Setter
//@NoArgsConstructor
//@ToString
//@Entity
//@Table(name = "Order")
//public class OrderEO implements Serializable {
//
//    private static final long serialVersionUID = -2576670215015463100L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
////    @OneToOne
////    @JoinColumn(name="account_id", referencedColumnName = "id")//nullable=false
////    private AccountEO accountEO;
//
//    @Column(nullable = false)
//    private String orderDate;
//
//    @Column(nullable = false, unique = true)
//    private Integer orderNum;
//
//    @Column(nullable = false)
//    private Double amount;
//
//}