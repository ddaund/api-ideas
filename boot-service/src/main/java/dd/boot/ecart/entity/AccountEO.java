package dd.boot.ecart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "Account")
public class AccountEO implements Serializable {

    private static final long serialVersionUID = -2054386655979281969L;

    public static final String ROLE_MANAGER = "MANAGER";
    public static final String ROLE_EMPLOYEE = "EMPLOYEE";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

   // @OneToOne//(mappedBy="accountEO")
   // private OrderEO orderEO;

    @Column(nullable = false)
    private String userName;

    @Column(nullable = false)
    private String encrytedPassword;

    @Column(nullable = false)
    private boolean active;

    @Column( nullable = false)
    private String userRole;

    @Column(nullable = false)
    private String customerName;

    @Column(nullable = false)
    private String address;

    @Column( nullable = false)
    private String email;

    @Column(nullable = false)
    private String phone;

}