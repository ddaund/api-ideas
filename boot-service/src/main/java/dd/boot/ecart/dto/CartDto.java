package dd.boot.ecart.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CartDto {

    private int orderNum;
    private CustomerDto customerDto;
    private final List<CartLineDto> cartLines = new ArrayList<>();
}