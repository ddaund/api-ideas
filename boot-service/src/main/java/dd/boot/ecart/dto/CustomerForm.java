package dd.boot.ecart.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class CustomerForm {

    @NotEmpty
    private String name;
    @NotEmpty
    private String address;
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    private String phone;

    private boolean valid;

    public CustomerForm(CustomerDto customerDto) {
        if (customerDto != null) {
            this.name = customerDto.getName();
            this.address = customerDto.getAddress();
            this.email = customerDto.getEmail();
            this.phone = customerDto.getPhone();
        }
    }
}