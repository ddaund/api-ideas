package dd.boot.ecart.dto;

import javax.validation.constraints.NotEmpty;

import org.springframework.web.multipart.MultipartFile;

import dd.boot.ecart.entity.ProductEO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductForm {

    @NotEmpty
    private String code;
    @NotEmpty
    private String name;
    @NotEmpty
    private double price;

    private boolean newProduct = false;

    // Upload file.
    private MultipartFile fileData;

    public ProductForm(ProductEO product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
    }
}