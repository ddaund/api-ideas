package dd.boot.ecart.dto;

import dd.boot.ecart.entity.ProductEO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ProductDto {
    private String code;
    private String name;
    private double price;

    public ProductDto(ProductEO product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
    }

    // Using in JPA/Hibernate query
    public ProductDto(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }
}