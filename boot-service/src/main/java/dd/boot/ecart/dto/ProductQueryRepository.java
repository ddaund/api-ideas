package dd.boot.ecart.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dd.boot.ecart.entity.ProductEO;

@Repository
public interface ProductQueryRepository extends JpaRepository<ProductEO, Long> {

//    @Query("SELECT new dd.boot.ecart.model.ProductLineDto(id, name, price) FROM ProductEO")
//    public List<ProductLineDto> projection();
}
