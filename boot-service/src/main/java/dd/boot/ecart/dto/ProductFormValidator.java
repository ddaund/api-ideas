package dd.boot.ecart.dto;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ProductFormValidator implements Validator {

//    @Autowired
//    private ProductDAO productDAO;

    // This validator only checks for the ProductForm.
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == ProductForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ProductForm productForm = (ProductForm) target;

        String code = productForm.getCode();
        if (productForm.isNewProduct()) {
//            Product product = productDAO.findProduct(code);
//            if (product != null) {
//                errors.rejectValue("code", "Duplicate.productForm.code");
//            }
        }
    }

}