package dd.boot.ecart.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CartLineDto {

    private ProductDto productDto;
    private int quantity;

    public double getAmount() {
        return this.productDto.getPrice() * this.quantity;
    }

}