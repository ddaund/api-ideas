package dd.boot.codevalue.web;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/codevalue")
public class CodeValueController {

	   @GetMapping("/")
	    @ResponseBody
	    public ResponseEntity<List<CodeValueDto>> getCodeValues() {
	       
	    	log.info("inside getCodeValues");
	    	CodeValueDto dto = new CodeValueDto();
	    	dto.setApplication("Dept");
	    	dto.setBusinessObject("Employee");
	    	dto.setBoProperty("gender");
	    	dto.setDescription("Gender of the Employee");
	    	
	    	CodeValueDto.PropertyValue femaleValue= dto.new PropertyValue();
	    	femaleValue.setEnumName("F");
	    	femaleValue.setLongName("Female");
	    	femaleValue.setEffectiveSince(LocalDate.now());
	    	femaleValue.setDefault(true);
	    	
	    	CodeValueDto.PropertyValue maleValue= dto.new PropertyValue();
	    	maleValue.setEnumName("M");
	    	maleValue.setLongName("Male");
	    	maleValue.setEffectiveSince(LocalDate.now());
	    	maleValue.setDefault(true);
	    	
	    	CodeValueDto.PropertyValue otherValue= dto.new PropertyValue();
	    	otherValue.setEnumName("O");
	    	otherValue.setLongName("Other");
	    	otherValue.setEffectiveSince(LocalDate.now());
	    	otherValue.setDefault(true);

	    	dto.setValues(Arrays.asList(maleValue, femaleValue, otherValue));	
	    	
	      //  List<CodeValueDto> bookDtos = (List<CodeValueDto>) entities.parallelStream().map(this::convert).collect(Collectors.toList());
	        return new ResponseEntity <> (Arrays.asList(dto), HttpStatus.OK);//200 OK
	    }

}
