package dd.boot.codevalue.web;

import java.time.LocalDate;
import java.util.List;

import lombok.Data;

@Data
public class CodeValueDto {

	private String application;
	private String businessObject;
	private String boProperty;
	private String description;
	private List<PropertyValue> values;
	
	@Data
	class PropertyValue{
		private String dbCode;
		private String label;
		private String enumName;
		private String longName;
		private LocalDate effectiveSince;
		private boolean isDefault;
	}
}
