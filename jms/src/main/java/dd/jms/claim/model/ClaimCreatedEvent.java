package dd.jms.claim.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Data
public class ClaimCreatedEvent {

    private Long claimId;
    private Long policyId;
    private String user;
    private LocalDate date;
}
