package dd.party.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import dd.party.entity.enums.ContactStatus;
import dd.party.entity.enums.ContactType;
import dd.party.entity.enums.PartyStatus;
import dd.party.entity.enums.PartyType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@DataJpaTest
public class PartyRepositoryTest {

    @Autowired
    private PartyRepository partyRepository;
    @Autowired
    private ContactRepository contactRepository;

    @Test
    public void repositoryLoad() {
        assertThat(partyRepository).isNotNull();
    }

    @Test
    public void test_insertParty() {
        PartyEO party = new PartyEO();
        party.setLongName("long-name");
        party.setShortName("short-name");
        party.setStatus(PartyStatus.A);
        party.setType(PartyType.FIL);
        ContactEO contactEO = new ContactEO();

        contactEO.setName("contactEO-name");
        contactEO.setStatus(ContactStatus.A);
        contactEO.setType(ContactType.DEL_CNT);
        contactEO.setPartyEO(party);
        party.setContactEOs(Collections.singletonList(contactEO));
        partyRepository.save(party);
        /*There is no default cascade type in JPA. By default no operations are cascaded.*/
        contactRepository.save(contactEO);
        log.info("debug point");
        log.info("********************************************************");
        log.info(partyRepository.findAll().toString());
        log.info("********************************************************");
        log.info(contactRepository.findAll().toString());
        assertThat(contactRepository.findAll()).hasSize(2);
    }

    @Test
    public void test_findAll() {
        assertThat(partyRepository.findAll()).hasSize(7);
    }

    @Test
    public void test_getOne() {
        assertThat(partyRepository.getOne(1001L)).isNotNull();
    }

    @Test
    public void test_findById() {
        assertThat(partyRepository.findById(1001L)).isNotNull();
    }

    @Test
    public void test_findByStatus() {
        assertThat(partyRepository.findByStatus(PartyStatus.A)).isNotEmpty();
        assertThat(partyRepository.findByStatus(PartyStatus.A)).hasSize(4);
    }
}
