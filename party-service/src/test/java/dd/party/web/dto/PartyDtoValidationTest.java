package dd.party.web.dto;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import dd.party.entity.enums.PartyCompany;

public class PartyDtoValidationTest {

    private static Validator validator;

    @BeforeAll
    public static void setup() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validate_ParytDto_thenAllPass() {

        PartyDTO partyDto = PartyDTO.builder().longName("Hanuman").company(PartyCompany.UGC)
                .roles(Collections.singletonList("PRO"))
                .address(AddressTO.builder().street1("street1").city("Pune").State("MH").zip("422045").build())
                .contacts(Collections.singletonList(
                        ContactTO.builder().name("Ram").type("UGC").emails(Collections.singletonList("ddaund@gmail.com")).build()))
                .build();
        Set<ConstraintViolation<PartyDTO>> voilations = validator.validate(partyDto);
        voilations.forEach(voilation -> System.out.println("****" + voilation));
        assertThat(voilations).size().isEqualTo(0);
    }

    @Test
    public void validate_ParytDto_thenAllFail() {

        Set<ConstraintViolation<PartyDTO>> voilations = validator.validate(PartyDTO.builder().build());
        voilations.forEach(System.out::println);
        assertThat(voilations).size().isEqualTo(4);
    }

    @Test
    public void validate_ContactDto_thenAllPass() {

        List<String> emails = new ArrayList<>();
        emails.add("ddaund@gmail.com");
        ContactTO validContactDto = ContactTO.builder().name("Ram").type("UGC").emails(emails).build();
        Set<ConstraintViolation<ContactTO>> voilations = validator.validate(validContactDto);
        assertThat(voilations).size().isEqualTo(0);
    }

    @Test
    public void validate_ContactDto_thenAllFail() {

        Set<ConstraintViolation<ContactTO>> voilations = validator.validate(ContactTO.builder().build());
        voilations.forEach(System.out::println);
        assertThat(voilations).size().isEqualTo(3);
    }

}
