package dd.party.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.json.JsonMapper;

import dd.boot.library.web.error.RestExceptionHandler;
import dd.party.service.PartyService;

@WebMvcTest
public class PartyControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private PartyService mockService;

    private JsonMapper jsonMapper;

    @BeforeAll
    public void setup() {
        jsonMapper = new JsonMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(new PartyController()).setControllerAdvice(new RestExceptionHandler()).build();
    }

    //@Test
    public void getPartyByPath_whenPartyPresent_ReturnPartyDto() throws Exception {

        //Given
        mockMvc.perform(get("/v1/parties/1"));
        //When
        ResultActions resultAction = mockMvc.perform(get("/v1/parties/1"));

        //Then
        resultAction.andExpect(status().isOk()).andDo(print());
    }

    public void getPartyByPath_whenPartyAbsent_Expect4xx() throws Exception {

        //Given

        //When
        ResultActions resultAction = mockMvc.perform(get("/v1/parties/101"));

        //Then
        resultAction.andExpect(status().is4xxClientError()).andDo(print());
    }

}
