package dd.party.service.mapper;

import dd.party.entity.ContactEO;
import dd.party.web.dto.ContactTO;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class ContactMapper {

    public ContactTO map(ContactEO contactEO){
        ContactTO contactTO = new ContactTO();
        contactTO.setId(contactEO.getId());
        contactTO.setName(contactEO.getName());
        contactTO.setEmails(Arrays.asList(contactEO.getEmailAddress().split(",")));
        return contactTO;
    }

    public ContactEO map(ContactTO contactTO){
        ContactEO contactEO = new ContactEO();
        contactEO.setId(contactTO.getId());
        contactEO.setName(contactTO.getName());
        contactEO.setEmailAddress(String.join(",", contactTO.getEmails()));
        return contactEO;
    }
}
