package dd.party.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dd.party.entity.PartyEO;
import dd.party.entity.PartyRepository;
import dd.party.entity.enums.PartyStatus;
import dd.party.entity.enums.PartyType;

@Service
public class PartyService {
    @Autowired
    private PartyRepository partyRepository;

    public PartyEO createParty(PartyEO party) {
        return partyRepository.save(party);
    }

    /**
     * inherited
     */
    public List<PartyEO> findAll() {
        return partyRepository.findAll();
    }

    public PartyEO getOne(Long id) {
        return partyRepository.getOne(id);
    }

    public PartyEO findById(Long id) {
        return partyRepository.findById(id).orElse(null);
    }

    /**
     * query methods
     */
    public List<PartyEO> findByStatus(PartyStatus status) {
        return partyRepository.findByStatus(status);
    }

    public List<PartyEO> findByType(PartyType type) {
        return partyRepository.findByType(type);
    }

    public List<PartyEO> findByShortName(String shortName) {
        return partyRepository.findByShortName(shortName);
    }

    public List<PartyEO> findBySbCode(String sbCode) {
        return partyRepository.findBySbCode(sbCode);
    }

    public List<PartyEO> findByTypeAndStatus(PartyType type, PartyStatus status) {
        return partyRepository.findByTypeAndStatus(type, status);
    }
}
