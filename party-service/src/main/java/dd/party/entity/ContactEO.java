package dd.party.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import dd.party.entity.enums.ContactStatus;
import dd.party.entity.enums.ContactType;
import lombok.Data;
import lombok.ToString;

@Data
@Table(name = "Contact")
@Entity
public class ContactEO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private AddressEO addressEO;

    /* example bi directional*/
    @ToString.Exclude
    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "party_id", nullable = false)
    private PartyEO partyEO;

    /*enums*/
    private ContactType type;
    private ContactStatus status;

    private String name;
    private String jobTitle;
    private String phone_number;
    private String fax_number;
    private String emailAddress;
    private String comment;
}
