package dd.party.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/*Example of one to many association with relation column type from party and interface_preference*/
@Entity
@Data
@Table(name = "PartyInterfacePreference")
@IdClass(PartyInterfacePreferenceCompositeKey.class)
public class PartyInterfacePreferenceEO {
    @Id
    @ManyToOne
    @JoinColumn(name = "party_id")
    private PartyEO partyEO;

    @Id
    @Column(name = "party_interface_type_id")
    /*change the type from Long to enum and save enumValue.getDbCode()*/
    private Long partyInterfaceTypeId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "interface_preference_id")
    private InterfacePreferenceEO interfacePreferenceEO;

}
