package dd.party.entity.tree.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PartyTreeStatusConverter implements AttributeConverter<PartyTreeStatus, String> {

    @Override
    public String convertToDatabaseColumn(PartyTreeStatus partyTreeStatus) {
        if (partyTreeStatus == null) {
            return null;
        }
        return partyTreeStatus.getDbCode();
    }

    @Override
    public PartyTreeStatus convertToEntityAttribute(String dbCode) {
        if (dbCode == null) {
            return null;
        }
        return PartyTreeStatus.forDbCode(dbCode);
    }
}