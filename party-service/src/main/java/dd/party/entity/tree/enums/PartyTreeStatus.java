package dd.party.entity.tree.enums;

import java.util.stream.Stream;

import lombok.Getter;

@Getter
public enum PartyTreeStatus {

    A("A", "Active"),
    I("I", "Inactive"),
    P("P", "Pending");

    private final String dbCode;
    private final String label;

    private PartyTreeStatus(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static PartyTreeStatus forDbCode(String dbCode) {
        return Stream.of(values())
                .filter(value -> value.getDbCode().equalsIgnoreCase(dbCode))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("dbCode is not valid for PartyTreeStatus enum"));
    }

    public static PartyTreeStatus forLabel(String label) {
        return Stream.of(values())
                .filter(value -> value.getLabel().equalsIgnoreCase(label))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("label is not valid for PartyTreeStatus enum"));
    }
}
