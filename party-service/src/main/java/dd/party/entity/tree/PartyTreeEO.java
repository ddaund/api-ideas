package dd.party.entity.tree;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import dd.party.entity.tree.enums.PartyTreeStatus;
import dd.party.entity.tree.enums.PartyTreeType;
import lombok.Data;

@Data
@Entity
@Table(name = "Party_Tree")
public class PartyTreeEO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "party_tree_effective_date")
    private LocalDate partyTreeEffectiveDate;

    private PartyTreeStatus status;
    private PartyTreeType type;

    private String shortName;
    private String longName;
    private String businessUnitId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumns({
            @JoinColumn(name = "party_tree_id", referencedColumnName = "id", updatable = false, insertable = true, nullable = true),
            @JoinColumn(name = "party_tree_effective_date", referencedColumnName = "party_tree_effective_date", updatable = false, insertable = true, nullable = true)
    })
    private List<PartyTreeNodeEO> partyTreeNodeEOs;
}
