package dd.party.entity.tree;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import dd.party.entity.tree.enums.PartyTreeNodeStatus;
import lombok.Data;

@Data
@Entity
@Table(name = "Party_Tree_Node")
@IdClass(PartyTreeNodeEOCompositeKey.class)
public class PartyTreeNodeEO {

    @Id
    @ManyToOne
    @JoinColumn(name = "party_tree_id", updatable = false, insertable = false)
    private PartyTreeEO partyTreeEO;

    @Id
    private Long childPartyId;

    @Id
    private Long parentPartyId;

    @Id
    @Column(name = "party_tree_effective_date")
    private LocalDate partyTreeEffectiveDate;

    @Id
    private Integer nodeQualifier;

    private PartyTreeNodeStatus status;

    private String relIdentifier;
}
