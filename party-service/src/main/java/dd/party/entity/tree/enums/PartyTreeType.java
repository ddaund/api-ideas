package dd.party.entity.tree.enums;

import java.util.stream.Stream;

import lombok.Getter;

@Getter
public enum PartyTreeType {

    OTHR(1, "Other");

    private final Integer dbCode;
    private final String label;

    PartyTreeType(Integer dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static PartyTreeType forDbCode(Integer dbCode) {

        return Stream.of(values())
                .filter(value -> value.getDbCode().equals(dbCode))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("dbCode is not valid for PartyTreeType enum"));

    }

    public static PartyTreeType forLabel(String label) {

        return Stream.of(values())
                .filter(value -> value.getLabel().equalsIgnoreCase(label))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("label is not valid for PartyTreeType enum"));
    }

}
