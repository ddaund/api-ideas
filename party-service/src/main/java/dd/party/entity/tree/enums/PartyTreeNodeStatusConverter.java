package dd.party.entity.tree.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PartyTreeNodeStatusConverter implements AttributeConverter<PartyTreeNodeStatus, String> {

    @Override
    public String convertToDatabaseColumn(PartyTreeNodeStatus partyStatus) {
        if (partyStatus == null) {
            return null;
        }
        return partyStatus.getDbCode();
    }

    @Override
    public PartyTreeNodeStatus convertToEntityAttribute(String dbCode) {
        if (dbCode == null) {
            return null;
        }
        return PartyTreeNodeStatus.forDbCode(dbCode);
    }
}