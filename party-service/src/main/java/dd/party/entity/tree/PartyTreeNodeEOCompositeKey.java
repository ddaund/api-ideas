package dd.party.entity.tree;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@SuppressWarnings("unused")
@AllArgsConstructor
@NoArgsConstructor
public class PartyTreeNodeEOCompositeKey implements Serializable {

	private static final long serialVersionUID = -4369582335971076806L;

	private Long partyTreeEO;
    private Long childPartyId;
    private Long parentPartyId;
	private LocalDate partyTreeEffectiveDate;
    private Integer nodeQualifier;

}
