package dd.party.entity.tree.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PartyTreeTypeConverter implements AttributeConverter<PartyTreeType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(PartyTreeType partyTreeType) {
        if (partyTreeType == null) {
            return null;
        }
        return partyTreeType.getDbCode();
    }

    @Override
    public PartyTreeType convertToEntityAttribute(Integer dbCode) {
        if (dbCode == null) {
            return null;
        }
        return PartyTreeType.forDbCode(dbCode);
    }
}