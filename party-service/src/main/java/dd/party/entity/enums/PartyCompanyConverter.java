package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PartyCompanyConverter implements AttributeConverter<PartyCompany, String> {

    @Override
    public String convertToDatabaseColumn(PartyCompany partyCompany) {

        if (partyCompany == null) return null;
        return partyCompany.dbCode();
    }

    @Override
    public PartyCompany convertToEntityAttribute(String dbCode) {

        return PartyCompany.valueOfDbCode(dbCode);
    }
}