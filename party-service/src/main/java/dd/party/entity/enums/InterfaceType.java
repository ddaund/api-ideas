package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum InterfaceType implements DbCodeEnum<Long> {

    ADR(10001l, "ADR"),
    ELECTRONIC_EOB(10002l, "EDIEOB"),
    MULTI_CLIENT_SPLIT_ADR(10003l, "MUSPADR"),
    INVESTING_PROCESSING(10004l, "INVPROC");

    private static final Map<Long, InterfaceType> BY_DB_CODE = new HashMap<>();
    private static final Map<String, InterfaceType> BY_LABEL = new HashMap<>();

    static {
        for (InterfaceType e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final Long dbCode;
    private final String label;

    private InterfaceType(Long dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static InterfaceType valueOfDbCode(Long dbCode) {

        return BY_DB_CODE.get(dbCode);
    }

    public static InterfaceType valueOfLabel(String label) {

        return BY_LABEL.get(label);
    }

    @Override
    public Long dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
