package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum InterfaceStatus implements DbCodeEnum<String> {
    ACTIVE("A", "Active"),
    INACTIVE("I", "Inactive");

    private static final Map<String, InterfaceStatus> BY_DB_CODE = new HashMap<>();
    private static final Map<String, InterfaceStatus> BY_LABEL = new HashMap<>();

    static {
        for (InterfaceStatus e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final String dbCode;
    private final String label;

    private InterfaceStatus(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static InterfaceStatus valueOfDbCode(String dbCode) {
        return BY_DB_CODE.get(dbCode);
    }

    public static InterfaceStatus valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    @Override
    public String dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
