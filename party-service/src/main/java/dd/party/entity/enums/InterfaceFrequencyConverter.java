package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class InterfaceFrequencyConverter implements AttributeConverter<InterfaceFrequency, String> {

    @Override
    public String convertToDatabaseColumn(InterfaceFrequency interfaceFrequency) {

        if (interfaceFrequency == null) return null;
        return interfaceFrequency.dbCode();
    }

    @Override
    public InterfaceFrequency convertToEntityAttribute(String dbCode) {
        return InterfaceFrequency.valueOfDbCode(dbCode);
    }
}