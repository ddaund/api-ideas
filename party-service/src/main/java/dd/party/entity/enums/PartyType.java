package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum PartyType implements DbCodeEnum<Integer> {
    FIL(1, "Delinquency Filer"),
    PROV(2, "Delinquency Provider");

    private static final Map<Integer, PartyType> BY_DB_CODE = new HashMap<>();
    private static final Map<String, PartyType> BY_LABEL = new HashMap<>();

    static {
        for (PartyType e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final Integer dbCode;
    private final String label;

    private PartyType(Integer dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static PartyType getDefault() {
        return PartyType.FIL;
    }

    public static PartyType valueOfDbCode(Integer dbCode) {

        return BY_DB_CODE.get(dbCode);
    }

    public static PartyType valueOfLabel(String label) {

        return BY_LABEL.get(label);
    }

    @Override
    public Integer dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
