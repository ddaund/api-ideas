package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class InterfaceMediaConverter implements AttributeConverter<InterfaceMedia, String> {

    @Override
    public String convertToDatabaseColumn(InterfaceMedia interfaceMedia) {

        if (interfaceMedia == null) return null;
        return interfaceMedia.dbCode();
    }

    @Override
    public InterfaceMedia convertToEntityAttribute(String dbCode) {

        return InterfaceMedia.valueOfDbCode(dbCode);
    }
}