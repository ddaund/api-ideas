package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PartyTypeConverter implements AttributeConverter<PartyType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(PartyType partyType) {

        if (partyType == null) return null;
        return partyType.dbCode();
    }

    @Override
    public PartyType convertToEntityAttribute(Integer dbCode) {
        return PartyType.valueOfDbCode(dbCode);
    }
}