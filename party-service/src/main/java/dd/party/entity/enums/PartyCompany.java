package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum PartyCompany implements DbCodeEnum<String> {

    UGC("UGC", "UGC"), ARCHMI("ARCHMI", "Arch MI");

    private static final Map<String, PartyCompany> BY_DB_CODE = new HashMap<>();
    private static final Map<String, PartyCompany> BY_LABEL = new HashMap<>();

    static {
        for (PartyCompany e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final String dbCode;
    private final String label;

    PartyCompany(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static PartyCompany valueOfDbCode(String dbCode) {
        return BY_DB_CODE.get(dbCode);
    }

    public static PartyCompany valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    @Override
    public String dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
