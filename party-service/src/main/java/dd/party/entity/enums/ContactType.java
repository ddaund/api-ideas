package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum ContactType implements DbCodeEnum<String> {
    DEL_CNT("DelCnt", "Delinquency Contact");

    private static final Map<String, ContactType> BY_DB_CODE = new HashMap<>();
    private static final Map<String, ContactType> BY_LABEL = new HashMap<>();

    static {
        for (ContactType e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final String dbCode;
    private final String label;

    private ContactType(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static ContactType valueOfDbCode(String dbCode) {
        return BY_DB_CODE.get(dbCode);
    }

    public static ContactType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    @Override
    public String dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
