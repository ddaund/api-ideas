package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class InterfaceStatusConverter implements AttributeConverter<InterfaceStatus, String> {

    @Override
    public String convertToDatabaseColumn(InterfaceStatus interfaceStatus) {

        if (interfaceStatus == null) return null;
        return interfaceStatus.dbCode();
    }

    @Override
    public InterfaceStatus convertToEntityAttribute(String dbCode) {

        return InterfaceStatus.valueOfDbCode(dbCode);
    }
}