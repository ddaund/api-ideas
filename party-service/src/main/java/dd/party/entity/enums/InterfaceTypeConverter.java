package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class InterfaceTypeConverter implements AttributeConverter<InterfaceType, Long> {

    @Override
    public Long convertToDatabaseColumn(InterfaceType interfaceType) {
        if (interfaceType == null) return null;
        return interfaceType.dbCode();
    }

    @Override
    public InterfaceType convertToEntityAttribute(Long dbCode) {
        return InterfaceType.valueOfDbCode(dbCode);
    }
}