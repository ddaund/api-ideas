package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum PartyStatus implements DbCodeEnum<String> {
    A("A", "Active"),
    I("I", "Inactive");

    private static final Map<String, PartyStatus> BY_DB_CODE = new HashMap<>();
    private static final Map<String, PartyStatus> BY_LABEL = new HashMap<>();

    static {
        for (PartyStatus e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final String dbCode;
    private final String label;

    private PartyStatus(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static PartyStatus getDefault() {
        return PartyStatus.I;
    }

    public static PartyStatus valueOfDbCode(String dbCode) {

        if (dbCode == null) return null;
        return BY_DB_CODE.get(dbCode);
    }

    public static PartyStatus forLabel(String label) {

        return BY_LABEL.get(label);
    }

    @Override
    public String dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
