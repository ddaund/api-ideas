package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ContactTypeConverter implements AttributeConverter<ContactType, String> {

    @Override
    public String convertToDatabaseColumn(ContactType contactType) {

        if (contactType == null) return null;
        return contactType.dbCode();
    }

    @Override
    public ContactType convertToEntityAttribute(String dbCode) {

        return ContactType.valueOfDbCode(dbCode);
    }
}