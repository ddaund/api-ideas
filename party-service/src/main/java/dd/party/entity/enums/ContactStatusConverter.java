package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ContactStatusConverter implements AttributeConverter<ContactStatus, String> {

    @Override
    public String convertToDatabaseColumn(ContactStatus contactStatus) {

        if (contactStatus == null) return null;
        return contactStatus.dbCode();
    }

    @Override
    public ContactStatus convertToEntityAttribute(String dbCode) {

        return ContactStatus.valueOfDbCode(dbCode);
    }
}