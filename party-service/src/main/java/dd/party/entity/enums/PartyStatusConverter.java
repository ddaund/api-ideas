package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PartyStatusConverter implements AttributeConverter<PartyStatus, String> {

    @Override
    public String convertToDatabaseColumn(PartyStatus partyStatus) {
        if (partyStatus == null) return null;
        return partyStatus.dbCode();
    }

    @Override
    public PartyStatus convertToEntityAttribute(String dbCode) {
        return PartyStatus.valueOfDbCode(dbCode);
    }
}