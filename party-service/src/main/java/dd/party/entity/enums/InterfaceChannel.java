package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum InterfaceChannel implements DbCodeEnum<Integer> {
    ELE(1, "Electronics"),
    MAN(2, "Manual");

    private static final Map<Integer, InterfaceChannel> BY_DB_CODE = new HashMap<>();
    private static final Map<String, InterfaceChannel> BY_LABEL = new HashMap<>();

    static {
        for (InterfaceChannel e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final Integer dbCode;
    private final String label;

    private InterfaceChannel(Integer dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static InterfaceChannel getDefault() {
        return InterfaceChannel.ELE;
    }

    public static InterfaceChannel valueOfDbCode(Integer dbCode) {
        return BY_DB_CODE.get(dbCode);
    }

    public static InterfaceChannel valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    @Override
    public Integer dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
