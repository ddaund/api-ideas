package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum ContactStatus implements DbCodeEnum<String> {
    A("A", "Active"),
    I("I", "Inactive"),
    P("P", "Pending");

    private static final Map<String, ContactStatus> BY_DB_CODE = new HashMap<>();
    private static final Map<String, ContactStatus> BY_LABEL = new HashMap<>();

    static {
        for (ContactStatus e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final String dbCode;
    private final String label;

    private ContactStatus(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static ContactStatus valueOfDbCode(String dbCode) {
        return BY_DB_CODE.get(dbCode);
    }

    public static ContactStatus valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    @Override
    public String dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
