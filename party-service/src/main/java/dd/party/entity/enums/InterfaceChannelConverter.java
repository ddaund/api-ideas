package dd.party.entity.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class InterfaceChannelConverter implements AttributeConverter<InterfaceChannel, Integer> {

    @Override
    public Integer convertToDatabaseColumn(InterfaceChannel interfaceChannel) {
        if (interfaceChannel == null) return null;
        return interfaceChannel.dbCode();
    }

    @Override
    public InterfaceChannel convertToEntityAttribute(Integer dbCode) {
        return InterfaceChannel.valueOfDbCode(dbCode);
    }
}