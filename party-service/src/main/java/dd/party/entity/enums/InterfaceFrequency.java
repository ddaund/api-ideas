package dd.party.entity.enums;

import java.util.HashMap;
import java.util.Map;

import dd.boot.library.entity.enums.DbCodeEnum;

public enum InterfaceFrequency implements DbCodeEnum<String> {

    MONTHLY("MO", "Monthly");

    private static final Map<String, InterfaceFrequency> BY_DB_CODE = new HashMap<>();
    private static final Map<String, InterfaceFrequency> BY_LABEL = new HashMap<>();

    static {
        for (InterfaceFrequency e : values()) {
            BY_DB_CODE.put(e.dbCode, e);
            BY_LABEL.put(e.label, e);
        }
    }

    private final String dbCode;
    private final String label;

    private InterfaceFrequency(String dbCode, String label) {
        this.dbCode = dbCode;
        this.label = label;
    }

    public static InterfaceFrequency valueOfDbCode(String dbCode) {
        return BY_DB_CODE.get(dbCode);
    }

    public static InterfaceFrequency valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    @Override
    public String dbCode() {
        return dbCode;
    }

    @Override
    public String label() {
        return label;
    }
}
