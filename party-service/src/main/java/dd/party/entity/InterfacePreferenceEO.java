package dd.party.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import dd.party.entity.enums.InterfaceChannel;
import dd.party.entity.enums.InterfaceFrequency;
import dd.party.entity.enums.InterfaceMedia;
import dd.party.entity.enums.InterfaceStatus;
import dd.party.entity.enums.InterfaceType;
import lombok.Data;

@Data
@Entity
@Table(name = "Interface_Preference")
public class InterfacePreferenceEO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /*enums*/
    private InterfaceChannel channel;
    private InterfaceType type;
    private InterfaceFrequency frequency;
    private InterfaceStatus status;
    private InterfaceMedia mediaType;

    private BigDecimal deviationPercentage;
    private long suspendAdrIndicator;
    private Integer monthWorkingDayNumber;

}
