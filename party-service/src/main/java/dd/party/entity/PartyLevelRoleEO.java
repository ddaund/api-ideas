package dd.party.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "party_level_role")
@IdClass(PartyLevelRoleCompositeKey.class)
public class PartyLevelRoleEO {

    @Id
    @ManyToOne()
    @JoinColumn(name = "party_id")
    private PartyEO partyRoleEO;

    @Id
    @Column(name = "partyRoleTypeId")
    private Long partyRoleTypeId;

}
