package dd.party.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import dd.party.entity.enums.PartyCompany;
import dd.party.entity.enums.PartyStatus;
import dd.party.entity.enums.PartyType;
import lombok.Data;

@Entity
@Data
@Table(name = "Party")
public class PartyEO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /*associations*/
    @OneToMany(mappedBy = "partyEO", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ContactEO> contactEOs;
    @OneToMany(mappedBy = "partyEO", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PartyAddressEO> partyAddressEOs;
    @OneToMany(mappedBy = "partyEO", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PartyInterfacePreferenceEO> partyInterfacePreferenceEOs;
    @OneToMany(mappedBy = "partyRoleEO", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<PartyLevelRoleEO> partyLevelRoleEOs;

    /*nonnull*/
    private int nonUpdateEmailInd;

    /*enums*/
    private PartyType type;
    private PartyStatus status;
    private PartyCompany company;

    private String shortName;
    private String longName;
    private String sbCode;
    private String businessId;
    private String customerNumber;

}
