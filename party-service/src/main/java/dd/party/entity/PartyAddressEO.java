package dd.party.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/*Example of many to many association relation between party and address*/
@Entity
@Data
@IdClass(PartyAddressCompositeKey.class)
@Table(name = "PartyAddress")
public class PartyAddressEO {

    @Id
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private AddressEO addressEO;

    @Id
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "party_id")
    private PartyEO partyEO;

    /*enums*/
    private String type;
    private String status;
}