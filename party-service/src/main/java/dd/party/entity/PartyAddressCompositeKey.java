package dd.party.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartyAddressCompositeKey implements Serializable {

	private static final long serialVersionUID = 721558590695927964L;

	private Long addressEO;
    private Long partyEO;

}
