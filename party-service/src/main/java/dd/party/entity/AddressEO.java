package dd.party.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "Address")
public class AddressEO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ToString.Exclude
    @OneToMany(mappedBy = "addressEO", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PartyAddressEO> partyAddressEOs;

    private String street1;
    private String street2;
    private String city;
    @Column(name = "state_province")
    private String stateProvince;
    private String country;
    private String zipPostalCode;
}