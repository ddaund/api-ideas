package dd.party.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import dd.party.entity.enums.PartyStatus;
import dd.party.entity.enums.PartyType;

@Repository
public interface PartyRepository extends JpaRepository<PartyEO, Long> {

    public List<PartyEO> findByStatus(PartyStatus status);

    List<PartyEO> findByType(PartyType type);

    /**
     * find by short name including null
     */
    @Query("select p from PartyEO p where (p.shortName is null or p.shortName=:shortName)")
    List<PartyEO> findByShortName(String shortName);

    //TODO This query does not work

    /**
     * find by sbCode if not null else find sbCode=null
     */
    @Query("select p from PartyEO p where p.sbCode = :sbCode or (:sbCode = null and p.sbCode = null)")
    List<PartyEO> findBySbCode(String sbCode);

    List<PartyEO> findByTypeAndStatus(PartyType type, PartyStatus status);
}