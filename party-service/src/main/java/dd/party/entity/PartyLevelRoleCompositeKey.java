package dd.party.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartyLevelRoleCompositeKey implements Serializable {

	private static final long serialVersionUID = 7815213693101312906L;
	
	//TODO what should be correct attribute name and type? partyRoleEO, partyRole, party_id
    private Long partyRoleEO;
    private Long partyRoleTypeId;

}
