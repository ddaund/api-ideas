package dd.party.web.error;

import org.springframework.web.bind.annotation.ControllerAdvice;

import dd.boot.library.web.error.RestExceptionHandler;

@ControllerAdvice(basePackages = "dd")
public class PartyControllerAdvice extends RestExceptionHandler {
}
