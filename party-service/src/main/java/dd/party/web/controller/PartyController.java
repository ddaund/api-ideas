package dd.party.web.controller;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dd.party.entity.enums.PartyStatus;
import dd.party.entity.enums.PartyType;
import dd.party.service.PartyService;
import dd.party.web.dto.PartyDTO;
import dd.party.web.dto.PartyMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/party")
public class PartyController {

    private final PartyMapper mapper = Mappers.getMapper(PartyMapper.class);
    @Autowired
    private PartyService partyService;

    @GetMapping
    public List<PartyDTO> findAll() {
        return mapper.toTransferObject(partyService.findAll());
    }

    @GetMapping("/{id}")
    @ResponseBody
    public PartyDTO getOne(@PathVariable("id") Long id) {
        return mapper.toTransferObject(partyService.getOne(id));
    }

    @GetMapping(params = "id")
    public PartyDTO findById(@RequestParam("id") Long id) {
        log.info("if id request param is not present control will not even come here");
        Assert.notNull(id, "request param id should not be null; results in IllegalArgumentException");
        Objects.requireNonNull(id, "request param id should not be null; results in NullPointerException");
        return mapper.toTransferObject(partyService.findById(id));
    }

    @GetMapping(params = "status")
    public List<PartyDTO> findByStatus(@RequestParam(name = "status", defaultValue = "Active") String status) {
        log.info("example demonstrate request param as enum and defaultValue");
        return mapper.toTransferObject(partyService.findByStatus(PartyStatus.forLabel(status)));
    }

    @GetMapping(params = "type")
    public List<PartyDTO> findByType(@RequestParam(name = "type") String type) {
        log.info("example demonstrate request param as enum and no default value hence null");
        return mapper.toTransferObject(partyService.findByType(PartyType.valueOfLabel(type)));
    }

    @GetMapping(params = "shortName")
    public List<PartyDTO> findByShortName(@RequestParam(name = "shortName") String shortName) {

        return mapper.toTransferObject(partyService.findByShortName(shortName));
    }

    @GetMapping(params = "sbCode")
    public List<PartyDTO> findBySbCode(@RequestParam(name = "sbCode") String sbCode) {

        return mapper.toTransferObject(partyService.findBySbCode(sbCode));
    }

    @GetMapping(params = {"status", "type"})
    public List<PartyDTO> getParties(@RequestParam(name = "status", defaultValue = "Active") String status,
                                     @RequestParam(name = "type") String type) {

        log.info("example of parameter default values and required/optional parameters");
        return mapper.toTransferObject(partyService.findByTypeAndStatus(PartyType.valueOfLabel(type), PartyStatus.forLabel(status)));
    }

    /**
     * wip below this....
     */
    @GetMapping("/filter")
    public List<PartyDTO> getPartiesWithMappingAllParam(@RequestParam Map<String, String> allParams) {
        log.info("multiple parameters without defining their names or count");
        log.info("Parameters are:{}", allParams.entrySet());
        return null;
    }

    @GetMapping("/multiValueParam")
    public List<PartyDTO> getPartiesWithMultiValueParam(@RequestParam List<String> ids) {

        Assert.notEmpty(ids, "ids must not be empty");
        log.info("Multi-Value parameters are:{}", ids);
        log.info("valid params are ( list of separate id parameters): " + "?id=1&id=2");
        log.info("valid params are (comma-delimited id parameter): " + "?id=1,2,3");

        return null;
    }
}