package dd.party.web.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import dd.party.entity.enums.PartyCompany;
import dd.party.entity.enums.PartyStatus;
import dd.party.entity.enums.PartyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartyDTO {

    private Long id;

    private int nonUpdateEmailInd;

    private PartyType type;
    private PartyStatus status;
    @NotNull
    private PartyCompany company;

    private String shortName;
    @NotEmpty
    private String longName;
    @Size(max = 3, message = "Maximum length allowed for sbCode is 3 characters")
    private String sbCode;
    private String businessId;
    private String customerNumber;
    private List<@Size(max = 10, min = 10) @Positive String> customerIds;

    private Boolean blastEmailIndicator;
    private String clientCode;
    private Boolean isNonSelfChildPresent;
    @NotEmpty
    private List<String> roles;
    @Valid
    @NotNull
    private AddressTO address;
    @Valid
    private List<ContactTO> contacts;
    private Long providerId;
    private String providerName;
    private Long treeId;
}
