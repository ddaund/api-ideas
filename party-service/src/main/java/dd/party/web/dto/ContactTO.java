package dd.party.web.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactTO {

    private Long id;

    //object relationship
    @Valid
    private AddressTO address;
    @NotBlank
    @Size(max = 40, message = "Maximum length allowed for name is 40 characters.")
    private String name;
    @Size(max = 50, message = "Maximum length allowed for name is 50 characters.")
    private String phone;
    private List<@Email String> emails;

    private String status;
    @NotBlank
    @Size(max = 40, message = "Maximum length allowed for type is 40 characters.")
    private String type;

    @AssertTrue // method return type should be boolean. wrapper class Boolean does not work!
    private boolean isPhoneOrEmailNotBlank() {
        return StringUtils.isNotBlank(phone) || !CollectionUtils.isEmpty(emails);
    }
}
