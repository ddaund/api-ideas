package dd.party.web.dto;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class AddressTO {

    @NotEmpty
    private String street1;
    private String street2;
    @NotEmpty
    private String city;
    @NotEmpty
    private String State;
    @NotEmpty
    private String zip;
    private String country;
}