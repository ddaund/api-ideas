package dd.party.web.dto;

import java.util.List;

import org.mapstruct.Mapper;

import dd.party.entity.PartyEO;

@Mapper(componentModel = "spring")
public interface PartyMapper {

    PartyEO toEntityObject(final PartyDTO partyTO);

    PartyDTO toTransferObject(final PartyEO partyEO);

    List<PartyDTO> toTransferObject(final List<PartyEO> partyEOList);
}