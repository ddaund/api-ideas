drop table if exists address CASCADE;
drop table if exists contact CASCADE;
drop table if exists interface_preference CASCADE;
drop table if exists party CASCADE;
drop table if exists party_level_role CASCADE;
drop table if exists party_address CASCADE;
drop table if exists party_interface_preference CASCADE;
drop sequence if exists hibernate_sequence;

create sequence hibernate_sequence start with 1 increment by 1;

create table address (
	id bigint not null,
    city varchar(255),
    country varchar(255),
    state_province varchar(255),
    street1 varchar(255),
    street2 varchar(255),
    zip_postal_code varchar(255),
    primary key (id)
);

create table contact (
   id bigint not null,
   comment varchar(255),
   email_address varchar(255),
   fax_number varchar(255),
   job_title varchar(255),
   name varchar(255),
   phone_number varchar(255),
   status varchar(255),
   type varchar(255),
   address_id bigint,
   party_id bigint not null,
   primary key (id)
);

create table interface_preference (
   id bigint not null,
   channel integer,
   deviation_percentage decimal(19,2),
   frequency bigint,
   media_type bigint,
   month_working_day_number integer,
   status bigint,
   suspend_adr_indicator bigint not null,
   type bigint,
   primary key (id)
);

create table party (
   id bigint not null,

   non_update_email_ind integer not null,

   type varchar(255),
   status varchar(255),
   company varchar(255),

   short_name varchar(255),
   long_name varchar(255),
   sb_code varchar(3),
   business_id varchar(255),
   customer_number varchar(255),

   primary key (id)
);

create table party_level_role (
   party_id bigint not null,
   party_role_type_id bigint not null,
   primary key (party_id, party_role_type_id)
);

create table party_address (
   address_id bigint not null,
   party_id bigint not null,
   status varchar(255),
   type varchar(255),
   primary key (address_id, party_id)
);

create table party_interface_preference (
   party_interface_type_id bigint not null,
   party_id bigint not null,
   interface_preference_id bigint,
   primary key (party_id, party_interface_type_id)
);

alter table contact
   add constraint FK_contact_address
   foreign key (address_id)
   references address;

alter table contact
   add constraint FK_contact_party
   foreign key (party_id)
   references party;

alter table party_level_role
   add constraint FK_partyLevelRole_party
   foreign key (party_id)
   references party;

alter table party_address
   add constraint FK_partyAddress_address
   foreign key (address_id)
   references address;

alter table party_address
   add constraint FK_partyAddress_party
   foreign key (party_id)
   references party;

alter table party_interface_preference
   add constraint FK_partyInterfacePreference_party
   foreign key (party_id)
   references party;

alter table party_interface_preference
   add constraint FK_partyInterfacePreference_interface_preference
   foreign key (interface_preference_id)
    references interface_preference;