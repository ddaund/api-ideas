
insert into party (id, non_update_email_ind, type, status, company, short_name, long_name, sb_code, business_id, customer_number) values
(1001, 0, 1, 'A', 'UGC', 'SBI', 'State Bank of India', 'ABC', null, null),
(1002, 0, 2, 'A', 'UGC', 'ICICI', 'ICICI Bank', '001', null, null),
(1003, 0, 1, 'A', 'UGC', 'IDBI', 'IDBI Bank',  'XYZ', null, null),
(1004, 0, 2, 'I', 'ARCHMI', 'HDFC', 'HDFC Bank', '111', null, null),
(1005, 0, 1, 'I', 'ARCHMI', null, 'Kotak Mahindra', 'SBC', null, null),
(1006, 0, 2, 'I', 'UGC', null, 'shortName and sbCode is null', null, null, null),
(1007, 0, 2, 'A', 'UGC', null, 'sbCode is null', null, null, null);

insert into contact (id, name, status, type, party_id) values
(1001, 'name1', 'A', 'DelCnt', 1001);

insert into address(id, street1, street2, city, state_province, country, zip_postal_code) values
(1, 'street1', 'street2', 'Los Angeles', 'Standford Ave', 'India', '90001');