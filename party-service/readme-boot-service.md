# boot-service
spring boot service


# party-service

Topics to cover
1. Bean validations
2. Using Spring ResponseEntity to Manipulate the HTTP Response
3. JPA Repository
4. JdbcTemplate - Native query complex joins ResultSet to DTO mapping avoiding Entity load
5. Exceptions
6. Entities to DTO mapping  (Dozer or BeanMapper)
7. jpa-jndi-ds
------------------
7. spring profiles (mock profile)
8. JUnit 5 Unit tests for controller, service and DAO/Repository
9. End to End integration (feature)  tests loading spring context
10. Entity Audit with hibernate envers
refer https://www.baeldung.com/spring-data-jpa-multiple-databases 
11. spring-boot 2+
12. handling special characters in H2 and JPA
13. Jenkinsfile in project




# spring
1. @DataJpaTest jpa projection
2. Spring Data JPA projections
3. left join, right join with projections
4. bean validation
5. accessing two or more RDBMS

Topics to cover
0. API security 
0. Using Spring ResponseEntity to Manipulate the HTTP Response
0. JdbcTemplate - Native query complex joins ResultSet to DTO mapping avoiding Entity load
0. Exceptions
0. Entities to DTO mapping  MapStruct
0. jpa-jndi-ds
------------------
0. spring profiles (mock profile)
0. JUnit 5 Unit tests for controller, service and DAO/Repository
0. End to End integration (feature)  tests loading spring context
0. Entity Audit with hibernate envers
refer https://www.baeldung.com/spring-data-jpa-multiple-databases 
0. handling special characters in H2 and JPA
0. Jenkinsfile in project

