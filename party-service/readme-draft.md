# api-service

Topics covered

--------JPA-------------------
1. Jpa Repository getOne() and exception handling in ControllerAdvice

Topics tobe covered
------------------------------------------------------------------
03. enum bean validation
-----------------------------------------------------------
05. Entity Audit with hibernate envers
06. JdbcTemplate - Native query complex joins ResultSet to DTO mapping avoiding Entity load
07. jpa-jndi-ds
09. Result to custom Dto
#10. multiple database access refer https://www.baeldung.com/spring-data-jpa-multiple-databases
7. spring profiles (mock profile)
12. handling special characters in H2 and JPA
13. Jenkinsfile in project
/////////////////////////////////////////////////////////////////////////////////////////////////
In query-api (read only)
1. have only projections (interface, class, dynamic). prefer class projections for performance
2. no create-update-delete operations
	use query.setHint(QueryHints.HINT_READONLY, true);
3. can have api to query multiple schema
4. report api goes here
5. findBy queries goes here..
JPA Criteria API vs JPQL https://www.objectdb.com/java/jpa/query/criteria   https://www.objectdb.com/java/jpa/query/jpql/select

minimal example to play with:****************************************8
https://www.logicbig.com/tutorials/spring-framework/spring-data/interface-based-projections.html

https://www.bytestree.com/spring/spring-data-jpa-projections-5-ways-return-custom-object/

https://thoughts-on-java.org/entities-dtos-use-projection/
https://thoughts-on-java.org/dto-projections/
https://stackoverflow.com/questions/53347063/why-are-interface-projections-much-slower-than-constructor-projections-and-entit
https://blog.arnoldgalovics.com/how-much-projections-can-help/

If performance is critical for a certain part of your application, use JPQL/Criteria API projections over Spring Data JPA ones as they have considerably better performance.

In command-api (create-update-delete)
1. majorly no queries for api client
2. be align to one database/schema
3. service and task to  modify records goes here
4. inserts/cleaup of staging tables of tasks goes here

Apis can be broadly clasified as
1. command services
2. query services
3. task services
	a. batch-jobs
	b. Async services ex. cleanup/purging of staging table

research
		jpa entities vs projections