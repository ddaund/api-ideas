# party-service
Demonstrate web layer and domain layer using RestController+[Delegate+Service]+Repository layers

============ All Layers =================
--------- common --------------
1. testing all layers
2. security with OAuth2 Sso token

=============== Web Layer =======================
---------------- RestController  ------------------
01. Request/Response Entity. - Using Spring ResponseEntity to Manipulate the HTTP Response
02. Bean Validation - enum (validate String status field for values "A", "I", "P")

03. error handling with http status
04. Mockito - Controller Unit Test
05. Mockito - Feature Test - loading spring context

============== Domain Layer ========================
--------------- Service Layer ----------------------
01. Its mostly pass through in this demo application as i am not going to do any serious business here.
02. MapStruct (maven dependency, maven-compiler-plugin annotation processor + lombok annotation processor also, plus i guess spring-boot-configuration-processor dependency)
    02.1 EntityObject (EO) conversion to DetailTransferObject (DTO). DTO generally/may have more attributes than EO mapped from associated EOs. Since DTO contains Detail information they will be mapped from EO
    02.2 Projections are by default View & TransferObject so let's call them (VTO) - NO conversion required. VTO holds less attributes but not limited to respective EO, so may contain some attributes from associated EO. Since VTO contains is kind of view they will be retried directly from tables. 
    Most important rule of thumb to decide DTO or VTO is, use VTO when information is required for read only purpose and not expecting subsequent edit or delete call. Else use DTO if you expect subsequent edit or delete call.
03. Mockito - Service Unit Test

---------------- Jpa layer -------------------------
01. @DataJpaTest
02. enum in Jpa entities
	: For DB - DbCode
	: For Java - enum name
	: For UI/service clients - label 
	: pending (I think now this is not required when enums used in Jpa; th git backup i thought initially for CodeTable, CodeValue table solution) - git backed descriptive info RestAPI
03. enum @Convertor

--in-progress
3. Jpa projections https://www.baeldung.com/jpa-hibernate-projections
    3.1 single column projections
    3.2 multi column projections
    3.3 projecting aggregate functions
    3.4 JPA CriteriaQuery API
4. hibernate projections
    4.1 single column projections
    4.2 multi column projections
    4.3 projecting aggregate functions
    4.4 using alias for projections
5. spring-data-jpa projections https://www.baeldung.com/spring-data-jpa-projections
    5.1 interface based projections
        5.1.1 closed projections
        5.1.2 open projections
    5.2 class based projections [Notice with the class-based approach, we cannot use nested projections.]
    5.3 dynamic projections [We can apply dynamic projections just by declaring a repository method with a Class parameter:]
              