package dd.boot.library.entity.enums;

public interface DbCodeEnum<T> {

    T dbCode();
    String label();
}
